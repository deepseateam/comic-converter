param (
    [Parameter(Mandatory=$false)][string]$inputFolder="comic",
    [Parameter(Mandatory=$false)][string]$ebookFormat="pdf",
    [Parameter(Mandatory=$false)][switch]$testMode=$False
)

Function CreateFolder($folder)
{
    If( !(Test-Path $folder) )
    {
        New-Item -ItemType Directory -Force -Path $folder
    }
}

Function ConvertComicPdf($bookFolder, $pdfFolder, $pdfFilename)
{
    $pdfPath = Join-Path $pdfFolder "tmp.pdf"
    $finalPdfPath = Join-Path $pdfFolder $pdfFilename

    # Convert images into single pdf file

    Write-Host "Convert $bookFolder to $pdfPath"

    $files = Get-ChildItem -Path $bookFolder -Recurse -File "*.jpg" | Sort-Object $ToNatural
    $command = "gm.exe"
    
    $argList = "convert"
    Foreach( $file in $files )
    {
        $argList += " $bookFolder\$($file.Name)"
    }
    
    $argList += " $pdfPath"

    # Start conversion

    $proc = Start-Process -filepath $command -argumentList $argList -Wait -PassThru -NoNewWindow

    # Rename output filename

    if(Test-Path -Path $finalPdfPath){
        Write-Host "Remove $finalPdfPath"
        Remove-Item -Path $finalPdfPath
    }

    Write-Host "Rename $pdfPath to $pdfFilename"
    Rename-Item -Path $pdfPath $pdfFilename
}

Function MobiCreateCoverImage($bookFolder, $imageFolder)
{
    $filePath = Join-Path $bookFolder "cover-image.jpg"

    $imageFiles = Get-ChildItem "$imageFolder\*.jpg"

    Move-Item -Path $imageFiles[0].FullName -Destination "$filePath"
}

Function MobiCreateTableOfContent($bookFolder, $imageFolder)
{
    $filePath = Join-Path $bookFolder "toc.ncx"
    $text = @"
<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE ncx PUBLIC '-//NISO//DTD ncx 2005-1//EN' 'http://www.daisy.org/z3986/2005/ncx-2005-1.dtd'>
<ncx version="2005-1" xmlns="http://www.daisy.org/z3986/2005/ncx/" xml:lang="en-US">
    <head>
        <meta content="" name="dtb:uid"/>
        <meta content="" name="dtb:depth"/>
        <meta content="0" name="dtb:totalPageCount"/>
        <meta content="0" name="dtb:maxPageNumber"/>
        <meta content="true" name="generated"/>
    </head>
    <docTitle>
        <text/>
    </docTitle>
    <navMap>
"@
    $imageFiles = Get-ChildItem "$imageFolder\*.jpg"

    for ($i=0; $i -lt $imageFiles.count; $i++) {
        $j = $i + 1

        $text += "`n        <navPoint playOrder=""$j"" id=""toc-$j"">"
        $text += "`n            <navLabel>"
        $text += "`n                <text>Page-$i</text>"
        $text += "`n            </navLabel>"
        $text += "`n            <content src=""html/Page-$i.html""/>"
        $text += "`n        </navPoint>"
    }
    $text += "`n    </navMap>"
    $text += "`n</ncx>"

    $text | Out-File $filePath
}

Function MobiCreateContentOpf($bookFolder, $imageFolder, $title, $creator)
{
    $filePath = Join-Path $bookFolder "content.opf"

    #$title = "book title"
    #$creator = "book creator"
    $text = @"
<package version="2.0" xmlns="http://www.idpf.org/2007/opf" unique-identifier="{955716de-3ecf-427b-be73-beecc262d0b6}">
    <metadata xmlns:opf="http://www.idpf.org/2007/opf" xmlns:dc="http://purl.org/dc/elements/1.1/">
        <meta content="comic" name="book-type"/>
        <meta content="true" name="zero-gutter"/>
        <meta content="true" name="zero-margin"/>
        <meta content="true" name="fixed-layout"/>
        <meta content="KindleComicCreator-1.0" name="generator"/>
"@

    $text += "`n        <dc:title>$title</dc:title>"
    $text += "`n        <dc:language>zh</dc:language>"
    $text += "`n        <dc:creator>$creator</dc:creator>"
    $text += @"
        <dc:publisher/>
        <dc:source>KC2/1.160/cf63ffb/win</dc:source>
        <meta content="portrait" name="orientation-lock"/>
        <meta content="horizontal-rl" name="primary-writing-mode"/>
        <meta content="800x1280" name="original-resolution"/>
        <meta content="false" name="region-mag"/>
        <meta content="cover-image" name="cover"/>
    </metadata>
    <manifest>
        <item href="toc.ncx" id="ncx" media-type="application/x-dtbncx+xml"/>
        <item href="cover-image.jpg" id="cover-image" media-type="image/jpg"/>
"@

    $imageFiles = Get-ChildItem "$imageFolder\*.jpg"

    for ($i=0; $i -lt $imageFiles.count; $i++) {
        $j = $i + 2
        $text += "`n        <item href=""html/Page-$i.html"" id=""item-$j"" media-type=""application/xhtml+xml""/>"
    }

    $text += "`n    </manifest>"
    $text += "`n    <spine toc=""ncx"">"

    for ($i=0; $i -lt $imageFiles.count; $i++) {
        $j = $i + 2
        $text += "`n        <itemref idref=""item-$j"" linear=""yes""/>"
    }

    $text += "`n    </spine>"
    $text += "`n</package>"

    $text | Out-File $filePath
}

Function MobiCreateHtml($bookFolder, $imageFolder)
{
    $htmlFolder = Join-Path "$bookFolder" "html"

    $imageFiles = Get-ChildItem "$imageFolder\*.jpg"


    for ($i=0; $i -lt $imageFiles.count; $i++) {
        $imageFile = $imageFiles[$i]
        $imageFilename = $imageFile.Name

        $filePath = Join-Path "$htmlFolder" "Page-$i.html"

        $text = @"
<!DOCTYPE html>
<html>
    <head>
        <meta content="KC2/1.160/cf63ffb/win" name="generator"/>
        <title></title>
    </head>
    <body>
        <div>
"@
        $text += "`n            <img style=""width:789px;height:1280px;margin-left:5px;margin-top:0px;margin-right:5px;margin-bottom:0px;"" src=""scaled-images/$imageFilename""/>"
        $text += @"
        </div>
    </body>
</html>
"@
        $text | Out-File $filePath
    }
}

Function ConvertComicMobi($bookFolder, $outputFolder, $outputFilename, $title, $creator)
{
    $htmlFolder = Join-Path $bookFolder "html"
    $imageFolder = Join-Path $htmlFolder "scaled-images"

    CreateFolder $htmlFolder
    CreateFolder $imageFolder

    # html\scaled-images
    Move-Item -Path "$bookFolder\*.jpg" -Destination "$imageFolder"

    #cover-image.jpg
    MobiCreateCoverImage $bookFolder $imageFolder

    #toc.ncx
    MobiCreateTableOfContent $bookFolder $imageFolder

    #content.opf
    MobiCreateContentOpf $bookFolder $imageFolder $title $creator

    #html\Page-X.html
    MobiCreateHtml $bookFolder $imageFolder

    .\kindlegen.exe "$bookFolder\content.opf" -o "output.mobi"
}

Function SplitImage($bookFolder, $tmpFolder)
{
    Write-Host "Split Images under $bookFolder"

    $files = Get-ChildItem -Path $bookFolder -File "*.mhr" | Sort-Object $ToNatural
    
    Write-Host "Files total: $($files.Length)"
    
    Foreach( $file in $files )
    {
        $imageFile = Join-Path $bookFolder $file.Name

        $output = (gm.exe identify "$imageFile") | Out-String
        $result = $output -match "([\d]+)x(\d+)\+0\+0"

        If( $result -eq $true )
        {
            $width = [double] $matches[1]
            $height = [double] $matches[2]
            $width2 = [int] $width/2
            $ratio = $width / $height
            
            If( $ratio -gt 1.1 )
            {
                # Split the image file into two image files

                Write-Output "Split $($file.Name)"

                $outputFile = "$tmpFolder\$($file.Name).2.jpg"
                $area = "{0}x{1}+0+0" -f $width2, $height
                gm.exe convert "$imageFile" -contrast -contrast -blur 1x0.5 -crop $area "$outputFile"


                $outputFile = "$tmpFolder\$($file.Name).1.jpg"
                $area = "{0}x{1}+{0}+0" -f $width2, $height
                gm.exe convert "$imageFile" -contrast -contrast -blur 1x0.5 -crop $area "$outputFile"
            }
            Else
            {
                Write-Output "Copy $($file.Name)"
                $outputFile = "$tmpFolder\$($file.Name).jpg"
                Copy-Item -Path $imageFile -Destination $outputFile
            }
        }

        #If( $testMode ) { Break }
    }
}

Function Main()
{
    $ToNatural = { [regex]::Replace($_, '\d+', { $args[0].Value.PadLeft(20) }) }
    $comicFolders = Get-ChildItem -Path $inputFolder -Directory

    foreach( $comicFolder in $comicFolders )
    {
        $comicName = $comicFolder.Name
        $comicPath = $comicFolder.FullName
        $books = Get-ChildItem -Path $comicFolder.FullName -Directory -Exclude "tmp" | Sort-Object $ToNatural

        $titleFile = ".\{0}\{1}\title.txt" -f $inputFolder, $comicName
        $title = Get-Content $titleFile

        $creatorFile = ".\{0}\{1}\creator.txt" -f $inputFolder, $comicName
        $creator = Get-Content $creatorFile

        for ($i=0; $i -lt $books.length; $i++) {
            $index = $i + 1
            $book = $books[$i]

            $bookSuffix = $index
            $ebookFolder = "{0}\{1}" -f $inputFolder, $comicName
            $ebookFilename = "{0}{1}.{2}" -f $title, $bookSuffix, $ebookFormat
            $ebookTitle = "$title $bookSuffix"
        
            $bookFolder = $book.FullName
            $tmpFolder = "{0}\tmp\{1}" -f $comicPath, $book.Name

            If( Test-Path $tmpFolder )
            {
                Remove-Item $tmpFolder -Recurse -ErrorAction Ignore
            }

            CreateFolder $tmpFolder

            SplitImage $bookFolder $tmpFolder

            If( $ebookFormat -eq "pdf" )
            {
        
                ConvertComicPdf $tmpFolder $ebookFolder $ebookFilename
            }
            Elseif ( $ebookFormat -eq "mobi" )
            {
                ConvertComicMobi $tmpFolder $ebookFolder $ebookFilename $ebookTitle $creator
            }

            If( $testMode ) { Break }
        }
    }
}

Main
