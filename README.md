# README #

An utility to convert comic files (e.g. jpg) to a single pdf file, such that it can be read on ebook device like kindle

# Prerequisite #

Download and install GraphicsMagick from http://www.graphicsmagick.org/download.html

# Usage #

File layout:

```
comicconverter.ps1
comic/books1
comic/books2
comic/title.txt
```

1. "comic" folder should be in english
2. "booksX" folder stores image files with extension ".mhr"
3. "title.txt" should contain the title of the comic

# Command #

Open powershell console, and run below command to generate pdf from image files

```
.\comicconverter.ps1
```
